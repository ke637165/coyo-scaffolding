/**
 * Jasmine Matchers that are written to simplify expectations and to increase test readability.
 * Usage:
 * expect(element(...)).toHaveClass('needle');
 * expect(element(...)).not.toHaveClass('missing-class');
 * expect(element(...)).toBeVisible();
 *
 * Prepend with "not" to negate the expression.
 *
 * Failing expectations will print a qualified message to the console.
 */
beforeEach(function () {
  'use strict';

  jasmine.addMatchers({
    toHaveClass: toHaveClassMatcher,
    toHaveText: toHaveTextMatcher,
    toContainText: toHavePartialTextMatcher,
    toBeVisible: toBeVisibleMatcher
  });

  /**
   * Expect the element to have the provided class set in it's "class" attribute.
   */
  function toHaveClassMatcher() {
    return {
      compare: function (actual, expectedClass) {
        var result = {};
        result.pass = actual.getAttribute('class').then(function (classes) {
          var pass = classes.split(' ').indexOf(expectedClass) !== -1;
          result.message = prepareCompareMessage(actual, 'to have class', expectedClass, classes, pass);
          return pass;
        });
        return result;
      }
    };
  }


  /**
   * Checks if the given ElementFinder isPresent() in the dom. If so, it checks if the ElementFinder isDisplayed().
   */
  function toBeVisibleMatcher() {
    // Checks given ElementFinder for isPresent() and isDisplayed(). Returns promise.
    function isVisible(actual) {
      var deferred = protractor.promise.defer(), result = {
        present: false,
        displayed: false
      };

      actual.isPresent().then(function (present) {
        if (present) {
          result.present = true;
          actual.isDisplayed().then(function (displayed) {
            result.displayed = displayed;
            deferred.fulfill(result);
          });
        } else {
          deferred.fulfill(result);
        }
      });
      return deferred.promise;
    }

    return {
      compare: function (actual) {
        var result = {};
        result.pass = isVisible(actual).then(function (visible) {
          var was = '[present: ' + visible.present + ', displayed: ' + visible.displayed + ']';
          var pass = visible.present && visible.displayed;
          result.message = prepareCompareMessage(actual, 'to be', 'visible', was, pass);
          return pass;
        });
        return result;
      }
    };
  }

  /**
   * Matches on text equality. The given text has to be exactly the same text that is returned by element.getText().
   * Textareas and input fields get checked against their "value" attribute.
   */
  function toHaveTextMatcher() {
    return textComparator(false);
  }

  /**
   * The given text has to be contained in the text that is returned by element.getText().
   * Textareas and input fields get checked against their "value" attribute.
   */
  function toHavePartialTextMatcher() {
    return textComparator(true);
  }

  function textComparator(partial) {
    return {
      compare: function (actual, expectedText, caseInsensitive) {
        var result = {};
        caseInsensitive = caseInsensitive !== false;

        result.pass = actual.getTagName().then(function (tag) {
          if (tag === 'input' || tag === 'textarea') {
            return actual.getAttribute('value').then(compareText);
          }
          return actual.getText().then(compareText);
        });

        function compareText(text) {
          if (caseInsensitive) {
            text = text.toUpperCase();
            expectedText = expectedText.toUpperCase();
          }
          var pass;
          if (partial) {
            pass = text.indexOf(expectedText) > -1;
          } else {
            pass = text === expectedText;
          }
          result.message =
            prepareCompareMessage(actual, !partial ? 'to have text' : 'to contain text', expectedText, text, pass);
          return pass;
        }

        return result;
      }
    };
  }

  // Prepare error messages that get printed on comparison errors, eg:
  // >  'Expected element[...] not to have class "bar" but was "foo bar baz".
  function prepareCompareMessage(actual, type, expected, was, not) {
    return 'Expected element [${locator}]\n\r\t${not}${type}"${expected}"\n\r\tbut was "${was}".'
        .replace('${locator}', actual.locator())
        .replace('${not}', (not ? 'not ' : ''))
        .replace('${type}', type + ' ')
        .replace('${expected}', expected)
        .replace('${was}', was);
  }

});
