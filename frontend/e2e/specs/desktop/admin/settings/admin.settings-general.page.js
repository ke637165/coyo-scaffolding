(function () {
  'use strict';

  var testhelper = require('../../../../testhelper.js');
  var Checkbox = require('../../../checkbox.page');

  module.exports = {
    get: function () {
      browser.get('/admin/settings/general');
      testhelper.disableAnimations();
    },
    enableDeletedUserAnonymization: new Checkbox(element(by.model('$ctrl.settings.deletedUserAnonymizationActive'))),
    saveButton: $('button[type="submit"]')
  };

})();
