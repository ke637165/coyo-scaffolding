(function () {
  'use strict';

  var Select = require('../../../select.page.js');
  var Checkbox = require('../../../checkbox.page');

  module.exports = {
    getById: function (id) {
      browser.get('/admin/authentication-providers/edit/' + id);
    },
    name: element(by.model('$ctrl.authenticationProvider.name')),
    type: new Select(element(by.model('$ctrl.authenticationProvider.type')), false),
    active: new Checkbox(element(by.model('$ctrl.authenticationProvider.active'))),
    saveButton: element(by.cssContainingText('.btn.btn-primary', 'Save')),
    isSaveButtonDisabled: function () {
      return this.saveButton.getAttribute('disabled').then(function (disabled) {
        return disabled === 'true';
      });
    },
    cancelButton: element(by.cssContainingText('.btn.btn-default', 'Cancel'))
  };

})();
