(function () {
  'use strict';

  var PersonalTimeline = require('./timeline-personal.page.js');
  var login = require('../../login.page.js');
  var path = require('path');
  var NavigationPage = require('../navigation.page.js');

  describe('personal timeline', function () {
    var personalTimeline, navigation;

    beforeAll(function () {
      personalTimeline = new PersonalTimeline();
      navigation = new NavigationPage();

      login.loginDefaultUser();
    });

    beforeEach(function () {
      navigation.getHome();
    });

    it('should load items', function () {
      expect(personalTimeline.stream.isPresent()).toBe(true);
      expect(personalTimeline.items.count()).toBeGreaterThan(0);
    });

    it('should focus the comment input field when entering the edit mode', function () {
      // given
      var timelineItem = personalTimeline.createTimelineItemWithComment();
      var comment = personalTimeline.findCommentsForTimelineItem(timelineItem).first();
      var editModeButton = personalTimeline.findSetEditModeButtonForComment(comment);

      // when
      personalTimeline.toggleCommentContextMenu(comment);
      editModeButton.click();

      // then
      var commentTextareaId = personalTimeline.findEditModeCommentInputForComment(comment).getAttribute('id');
      var focusedElementId = browser.driver.switchTo().activeElement().getAttribute('id');
      var saveButton = personalTimeline.findEditSaveButtonForComment(comment);
      var cancelButton = personalTimeline.findEditCancelButtonForComment(comment);
      expect(commentTextareaId).toEqual(focusedElementId);
      expect(saveButton.isDisplayed()).toBe(true);
      expect(cancelButton.isDisplayed()).toBe(true);

      // cleanup
      cancelButton.click();
      personalTimeline.deleteTimelineItemUsingContextMenu(timelineItem);
    });

    it('should edit comments of timeline items', function () {
      var timelineItem = personalTimeline.createTimelineItemWithComment();
      var comment = personalTimeline.findCommentsForTimelineItem(timelineItem).first();
      var editModeButton = personalTimeline.findSetEditModeButtonForComment(comment);
      personalTimeline.toggleCommentContextMenu(comment);
      editModeButton.click();
      var commentTextarea = personalTimeline.findEditModeCommentInputForComment(comment);
      var saveButton = personalTimeline.findEditSaveButtonForComment(comment);
      var changedText = 'Changed that.';

      // when
      commentTextarea.clear().sendKeys(changedText);
      saveButton.click();

      // then
      expect(saveButton.isPresent()).toBe(false);
      var message = personalTimeline.findCommentMessageElement(comment);
      expect(message).toHaveText(changedText);

      // cleanup
      personalTimeline.deleteTimelineItemUsingContextMenu(timelineItem);
    });

    it('should load timeline item comments', function () {
      var comments = personalTimeline.findCommentsForTimelineItem(personalTimeline.items.first());
      expect(comments.count()).toBeGreaterThan(0);
    });

    // eslint-disable-next-line jasmine/no-disabled-tests
    xit('create a timeline post with attachment', function () {
      var message = 'Hello World ' + Math.floor(Math.random() * 1000000);
      personalTimeline.form.messageField.sendKeys(message);

      var fileToUpload = 'upload.txt';
      var absolutePath = path.resolve(__dirname, fileToUpload); //eslint-disable-line
      console.log('Uploading file from: ', absolutePath); //eslint-disable-line

      personalTimeline.form.attachmentTrigger.click();
      $('input[type=file]').sendKeys(absolutePath);


      browser.sleep(300); // wait a little bit
      expect(element(by.cssContainingText('.timeline-item-message', message)).isPresent()).toBe(true);
      expect(element(by.cssContainingText('.timeline-item-attachment-name', fileToUpload)).isPresent()).toBe(true);
    }).pend('test cannot run in docker env as upload file is not available');
  });

})();
