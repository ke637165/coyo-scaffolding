(function () {
  'use strict';

  var TimelineItem = require('./timeline-item.page');

  function SenderTimeline() {
    var api = this;

    api.stream = $('.timeline-stream');
    api.loadMore = $('button[ng-click="$ctrl.loadMore()"]');
    api.timelineItems = TimelineItem.getAllElements(api.stream);
    api.form = {
      container: element(by.css('.timeline-form-inline')),
      messageField: element(by.css('.timeline-form-inline textarea[name=message]')),
      attachmentTrigger: element(by.css('.timeline-form-attachment-trigger')),
      submitBtn: element(by.css('.timeline-form-inline button[type=submit]'))
    };
    api.getTimelineItem = function (index) {
      return TimelineItem.create(api.stream, index);
    };
  }

  module.exports = SenderTimeline;

})();
