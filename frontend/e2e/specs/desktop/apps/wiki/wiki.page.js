(function () {
  'use strict';

  var Checkbox = require('./../../../checkbox.page');

  var WikiPage = function () {
    var api = this;
    var createWikiApp = $('.create-app-form');
    var wikiApp = '.wiki-app';

    api.setting = {
      appName: createWikiApp.$('#app-name-NONE'),
      active: new Checkbox(createWikiApp.element(by.model('$ctrl.app.active')))
    };

    api.articleList = {
      empty: $('span[translate="APP.WIKI.EMPTY"]'),
      list: $(wikiApp + '.article-tree'),
      create: $('.wiki-article-create'),
      articles: $('.article-tree .angular-ui-tree-nodes').all(by.css('.tree-node')),
      articleContextMenu: '.wiki-article .context-menu .dropdown .dropdown-toggle .dropdown-menu-right',
      leftOption: 'left-options',
      contextMenu: {
        hover: by.css('.wiki-article-node'),
        toggle: by.css('.context-menu.dropdown.dropdown-toggle '),
        edit: by.css('a[ui-sref=".view({id:node.id, editMode:true})"]'),
        delete: by.css('a[ng-click="ctrl.deleteArticle(node)"]')
      }
    };

    api.article = {
      create: {
        header: $('.wiki-article-header'),
        titleInput: $('.wiki-article-title #title-NONE'),
        editorInput: $('.rte-widget .note-editable'),
        saveButton: $('.article-save '),
        saveEditButton: $('.btn-primary'),
        editTitle: $('.article-header-top .editable')
      },
      view: {
        title: $('.article-header-top .panel-title'),
        text: $('.rte-html-container'),
        overView: element(by.css('.breadcrumb-list-bulleted a[ui-sref="^"]'))
      }
    };
  };

  module.exports = WikiPage;

})();
