(function () {
  'use strict';

  var login = require('../../login.page.js');
  var Navigation = require('./../navigation.page.js');
  var Profile = require('./../profile/profile.page.js');
  var Notification = require('./notifications.page.js');

  describe('notification discussion', function () {
    var navigation, profile, notification;

    beforeAll(function () {
      login.loginDefaultUser();
      navigation = new Navigation();
      profile = new Profile();
      notification = new Notification();
    });

    it('create notification and read it', function () {
      // write a message on Robert Lang activity wall
      var message = 'Hey I have a great new idea';
      var comment = 'It is a really good idea';
      profile.wall.openForUser('robert-lang');
      profile.wall.postOnWallTextarea.sendKeys(message);
      profile.wall.postOnWallSubmit.click();
      var commentTextarea = profile.wall.commentOnWallTextarea;
      commentTextarea.sendKeys(comment);
      profile.wall.submitComment.click();

      login.logout();
      // login as Robert Lang to check the notification
      login.login('rl', 'demo');
      navigation.notification.open();
      navigation.notification.discussion.click();
      navigation.notification.notifications.get(0).click();
      expect(notification.notificationPage.timelineItem.getText()).toBe(message);

      var comments = notification.notificationPage.comments;
      for (var i = 0; i < comments.length; ++i) {
        var item = comments.get(i).getText();
        if (item === comment) {
          expect(item).toEqual(comment);
        }
      }
    });

  });
})();
