(function () {
  'use strict';

  var extend = require('util')._extend;

  function MediaWidget(widget) {
    var api = extend(this, widget);

    api.settings = {
      media: element(by.model('model.settings._media')),
      album: {
        title: element(by.model('model.settings.album.title')),
        description: element(by.model('model.settings.album.description')),
        location: element(by.model('model.settings.album.location'))
      }
    };

    api.mediaTab = $('ul li.media-tab');
    api.informationTab = $('ul li.information-tab');

    api.changeTitle = function (title) {
      element(by.css('input#album-title')).clear().sendKeys(title);
    };
    api.changeDescription = function (description) {
      element(by.css('textarea#album-description')).clear().sendKeys(description);
    };
    api.changeLocation = function (location) {
      element(by.css('input#album-location')).clear().sendKeys(location);
    };

    api.openFileLibrary = function () {
      element(by.css('div.file-library-button a')).click();
    };

    api.goToFileLibraryHome = function () {
      element(by.css('ul.fl-breadcrumbs li:first-child')).click();
    };

    api.goToMyFiles = function () {
      element(by.css('.fl-table .fl-table-row:first-child')).click();
    };

    api.closeLightBoxButton = element(by.css('span.modal-close'));

    api.selectExampleFile = function () {
      api.openFileLibrary();
      api.goToFileLibraryHome();
      api.goToMyFiles();
      element(by.css('.file > .fl-table-cell:first-child')).click();
      element(by.css('.modal-footer button')).click();
    };

    api.renderedWidget = {
      albumTitle: element(by.css('.media-widget .heading .title')),
      albumDescription: element(by.css('.media-widget .heading .description')),
      albumLocation: element(by.css('.media-widget .heading .location')),
      //media: element(by.css('.media-tiles coyo-image-reference picture'))
      media: $$('.media-tiles').get(0)
    };
  }

  module.exports = MediaWidget;
})();
