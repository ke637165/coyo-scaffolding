(function () {
  'use strict';

  function Select(container, multiple) {
    this.openDropdown = function () {
      // container.$('input[type="search"]').click();
      container.$(multiple ? '.ui-select-search' : '.ui-select').click();
    };

    this.selectOption = function (label) {
      element(by.cssContainingText('.ui-select-choices-row', label)).click();
    };

    this.search = function (term) {
      this.openDropdown();
      $('.ui-select-container input[type="search"]').sendKeys(term);
    };

    this.selectedOptions = function () {
      return container
          .$$('.ui-select-match-item .ng-binding')
          .map(function (item) {
            return item.getText();
          });
    };

    this.removeOption = function (label) {
      container
          .element(by.cssContainingText('.ui-select-match-item', label))
          .$('.ui-select-match-close')
          .click();
    };
  }

  module.exports = Select;

})();
