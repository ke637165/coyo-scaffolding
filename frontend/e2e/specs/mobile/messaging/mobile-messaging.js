(function () {
  'use strict';

  var login = require('../../login.page.js');
  var MobileNavigation = require('../mobile-navigation.page');

  describe('mobile messaging', function () {
    var mobileNavigation;

    beforeEach(function () {
      mobileNavigation = new MobileNavigation();
      login.loginDefaultUser();
    });

    it('open and close messaging', function () {
      // open messaging sidebar
      mobileNavigation.messaging.open();
      // expect that messaging sidebar is open/side-in
      expect(mobileNavigation.messaging.messagingbar.isPresent()).toBeTruthy();

      // close messaging module
      mobileNavigation.messaging.close();
      expect(mobileNavigation.messaging.messagingbar.isPresent()).toBeFalsy();
    });
  });

})();
