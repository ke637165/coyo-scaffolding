import {$, browser, By, ElementFinder, ExpectedConditions as EC} from 'protractor';
import {promise as wdpromise} from 'selenium-webdriver';
import {BaseArrayFragment, BaseFragment} from 'protractor-element-extend';
import {Checkbox, ContextMenu, Dialog, Form, Select, TextField} from '../components/Elements';
import {Action, ActionHolder, Actions} from '../components/Action';
import {applyMixins} from '../../../TypescriptUtil';
import {TestHelper, XPathUtil} from '../../Util';

export class CommentForm extends BaseFragment implements Form<CommentForm> {
    // TODO attachFiles
    public message: TextField = new TextField('message', this.$('.comments-form textarea[name="message"]'));

    // inherited from HasActions
    public actions: ActionHolder = new ActionHolder();
    public isActionSelectable: (action: Actions | string) => wdpromise.Promise<boolean>;
    public setActions: (actions?: Action[]) => CommentForm;
    public addAction: (actionGenerator: (component: CommentForm) => Action) => CommentForm;
    public selectAction: (action: Actions | string) => CommentForm;
    // inherited from Form
    public fillForm: (data: any | any[]) => CommentForm;
    public doSubmit: () => CommentForm;

    constructor(elementFinder: ElementFinder) {
        super(elementFinder);
        this.addAction((form) => Action.submitButton(form));
    }
}

applyMixins(CommentForm, [Form]);

export class Comment extends BaseFragment {
    // TODO author, when, like: Button, likes
    public message: ElementFinder = this.$('.comment-message');

    /*
        contextMenu: ContextMenu = new ContextMenu(this.$('.comment-context-menu'), [
            new Action(Actions.Edit, 'li a:has(> i[class~=zmdi-edit])')]);
    */

    public isEditable(): wdpromise.Promise<boolean> {
        return this.contextMenu.isActionSelectable(Actions.Edit);
    }
}

export class Comments extends BaseFragment {
    public loadMore: Action = new Action('loadMore', this.$('a[translate="COMMENTS.LOAD_MORE"]'));
    public form: CommentForm = new CommentForm(this.$('.comments-form'));
    public postTimeout: number = 5000; // ms

    public items(): BaseArrayFragment<Comment> {
        return new BaseArrayFragment<Comment>(this.$$('.comment'), Comment);
    }

    public postComment(message: string): Comments {
        this.form.fillForm({message}).doSubmit();
        return this;
    }

    public expectNewestCommentToBe(message: string): Comments {
        browser.wait(EC.textToBePresentInElement(this.items().first(), message), this.postTimeout);
        return this;
    }
}

export class TimelineForm extends BaseFragment implements Form<TimelineForm> {
    public message: TextField = new TextField('message', this.$('#message'));
    public sender: Select = new Select('sender', this.$('')); // FIXME
    public restricted: Select = new Select('lock', this.$('')); // FIXME

    // inherited from HasActions
    public actions: ActionHolder = new ActionHolder();
    public isActionSelectable: (action: Actions | string) => wdpromise.Promise<boolean>;
    public setActions: (actions?: Action[]) => TimelineForm;
    public addAction: (actionGenerator: (component: TimelineForm) => Action) => TimelineForm;
    public selectAction: (action: Actions | string) => TimelineForm;
    // inherited from Form
    public fillForm: (data: any | any[]) => TimelineForm;
    public doSubmit: () => TimelineForm;

    constructor(elementFinder: ElementFinder) {
        super(elementFinder);
        this.addAction((form) => Action.submitButton(form));
    }
}

applyMixins(TimelineForm, [Form]);

export class TimelineItemLike extends Checkbox {
    constructor(elementFinder: ElementFinder) {
        super('like', elementFinder, elementFinder.$('.btn-likes-handle'),
            () => TestHelper.hasClass(elementFinder, 'active'));
    }
}

export class TimelineEditDialog extends Dialog<TimelineEditDialog> implements Form<TimelineEditDialog> {
    public message: TextField = new TextField('message', this.$('#message'));

    // inherited from HasActions
    public actions: ActionHolder = new ActionHolder();
    public isActionSelectable: (action: Actions | string) => wdpromise.Promise<boolean>;
    public setActions: (actions?: Action[]) => TimelineEditDialog;
    public addAction: (actionGenerator: (component: TimelineEditDialog) => Action) => TimelineEditDialog;
    public selectAction: (action: Actions | string) => TimelineEditDialog;
    // inherited from Form
    public fillForm: (data: any | any[]) => TimelineEditDialog;
    public doSubmit: () => TimelineEditDialog;

    constructor() {
        super($('.timeline-edit-modal'));
        this.addAction((dialog) => Action.submitButton(dialog))
            .addAction((dialog) => new Action(Actions.Cancel, dialog.$('a[translate="CANCEL"]')));
    }
}

applyMixins(TimelineEditDialog, [Form]);

export class TimelineItem extends BaseFragment {
    private static readonly xpathTimelineEdit = './/li//a[' + XPathUtil.hasChild('*', XPathUtil.hasClass('zmdi-edit')) + ']';

    // TODO implement sender, share, subscription, burger-menu report/delete
    public message: ElementFinder = this.$('.timeline-item-message');
    public wasEdited: ElementFinder = this.$('*[translate="MODULE.TIMELINE.EDIT.REMARK"]');
    public like: TimelineItemLike = new TimelineItemLike(this.$('.btn-likes'));
    public contextMenu: ContextMenu = new ContextMenu(this.$('.status-options .context-menu'))
        .addAction((menu) => new Action(Actions.Edit, menu.element(By.xpath(TimelineItem.xpathTimelineEdit))));
    public comments: Comments = new Comments(this.$('coyo-comments'));
    public editDialog: TimelineEditDialog = new TimelineEditDialog();

    public setEditMode(): TimelineItem {
        this.contextMenu.selectAction(Actions.Edit);
        return this;
    }
}

export class Timeline extends BaseFragment {
    public form: TimelineForm = new TimelineForm(this.$('.timeline-form-inline'));
    public postTimeout: number = 5000; // ms

    constructor() {
        super($('.timeline-stream'));
    }

    public items(): BaseArrayFragment<TimelineItem> {
        return new BaseArrayFragment<TimelineItem>(this.$$('.timeline-item'), TimelineItem);
    }

    public postMessage(message: string): Timeline {
        this.form.fillForm({message}).selectAction(Actions.Submit);
        return this;
    }

    public postMessageAndExpect(message: string): Timeline {
        return this
            .postMessage(message)
            .expectNewestMessageToBe(message);
    }

    public expectNewestMessageToBe(message: string): Timeline {
        browser.wait(EC.textToBePresentInElement(this.items().first(), message), this.postTimeout);
        return this;
    }
}
